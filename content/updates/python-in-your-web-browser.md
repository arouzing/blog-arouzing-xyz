---
title: Python in your web browser!
date: 2022-05-06T14:15:18.800Z
type: post
draft: false
description: pyscript changing the web!
---
<link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
<script defer src="https://pyscript.net/alpha/pyscript.js"></script> 

# Goodbye JavaScript hello Python

By simply adding this code to your website, you can run Python in the browser!
```html
<link rel="stylesheet" href="https://pyscript.net/alpha/pyscript.css" />
<script defer src="https://pyscript.net/alpha/pyscript.js"></script> 
```
<py-script>
print("this was done in pyhon!")
</py-script>|